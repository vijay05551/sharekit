//
//  AppDelegate.h
//  ShareKit
//
//  Created by osx on 01/06/16.
//  Copyright © 2016 Ameba Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

